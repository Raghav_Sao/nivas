var app = angular.module('myApp', ['ngRoute']);

app.config(function($routeProvider, $interpolateProvider){
	$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
	$routeProvider.
		when('/index', {
			templateUrl: '/bundles/nivasweb/js/app/templates/index.html',
			controller: 'IndexController'
		}).
		when('/aboutus', {
			templateUrl: '/bundles/nivasweb/js/app/templates/aboutus.html',
			controller: 'AboutusController'
		}).
		when('/contactus', {
			templateUrl: '/contactus/enquiry',
			controller: 'ContactusController as ctrl'
		}).
		when('/services', {
			templateUrl: '/bundles/nivasweb/js/app/templates/services.html',
			controller: 'ServicesController'
		}).
		when('/vendorregistration', {
			templateUrl: '/bundles/nivasweb/js/app/templates/vendorregistration.html',
			controller: 'VendorRegistrationController'
		}).
		when('/career', {
			templateUrl: '/bundles/nivasweb/js/app/templates/career.html',
			controller: 'CareerController'
		}).
		when('/helpinghands', {
			templateUrl: '/bundles/nivasweb/js/app/templates/helpinghands.html',
			controller: 'HelpingHandsController'
		}).
		otherwise({
			templateUrl: '/bundles/nivasweb/js/app/templates/index.html',
			controller: 'IndexController'
		});
		
});