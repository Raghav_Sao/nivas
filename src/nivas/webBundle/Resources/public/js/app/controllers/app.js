app.controller("AboutusController", function($scope) {
    
});
var _proto = ContactusController.prototype;
app.controller("ContactusController", ContactusController); 

function ContactusController($scope) {
var thisView = this;

/*
    Validate Email Method
*/
function validateEmail(Email) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(Email)) {
        return true;
    } else { 
        return false;
    }
}

/*
    Enquiry Form
*/

$('#enquiry_submit_btn').click(function() {
	$('#error').text('');
	$('#success').text('');
    $('#enquiry_submit_btn').attr('disabled','disabled');
    var postdata = $('.enquiry_form').serialize();
    var email = $("[name='email']").val().trim();
    var name = $("[name='name']").val().trim();
    var type = $("[name='type']").val().trim();
    var enquiry = $("[name='enquiry']").val().trim();
    if(name && email && validateEmail(email) && type && enquiry) {
    	$.ajax({
            type: 'POST',
            url: 'contactus/enquiry',
            data: postdata,
            dataType: 'json',
            success: function(response) {
                if(typeof(response.message) !== 'undefined') {
                    console.log(response.message);
                }
                $('#success').text("Thanks. We'll get back to you soon.");
                $("[name='email']").val('');
			    $("[name='name']").val('');
			    $("[name='type']").val('');
    			$("[name='enquiry']").val('');
            },
            error: function(response) {
                if(typeof(response.error) !== 'undefined') {
                    console.log(response.error);
                    $('#error').text("Some error occured. Please fill all the details and submit again.");
                }
            }
        });
    } else {
    	error = '';
    	if (!name ) {
	    	error = 'Name is mandatory field';
	    } else if (!email) {
	    	error = 'Email is mandatory field';
	    } else if (!validateEmail(email)) {
	    	error = 'Email is invalid';
	    } else if (!type ) {
	    	error = 'Please select Event Type';
	    } else if (!enquiry ) {
	    	error = 'Please provide any description';
	    }
	    $('#error').text(error);
	    // if (!name ) {
	    // 	thisView.name_error = 'Name is mandatory field';
	    // 	alert(thisView.name_error);
	    // }
	    // if (!email) {
	    // 	thisView.email_error = 'Email is mandatory field';
	    // 	alert(thisView.email_error);
	    // } else if (!validateEmail(email)) {
	    // 	thisView.email_error = 'Email is invalid';
	    // 	alert(thisView.email_error);
	    // } 
	    // if (!type ) {
	    // 	thisView.type_error = 'Please select Event Type';
	    // 	alert(thisView.type_error);
	    // } 
	    // if (!enquiry ) {
	    // 	thisView.enquiry_error = 'Enquiry is mandatory field';
	    // 	alert(thisView.enquiry_error);
	    // }
	}
	$('#enquiry_submit_btn').removeAttr('disabled');
    });
}
app.controller("ServicesController", function($scope) {
    
});
app.controller("VendorRegistrationController", function($scope) {
    
});
app.controller("CareerController", function($scope) {
    
});
app.controller("HelpingHandsController", function($scope) {
    
});
app.controller("IndexController", function($scope) {
    
});

app.controller("PanelController",function($scope, $location) {
	this.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };
});







