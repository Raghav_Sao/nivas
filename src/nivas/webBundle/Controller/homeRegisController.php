<?php

namespace nivas\webBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use nivas\webBundle\Entity\homeRegis;
use nivas\webBundle\Form\homeRegisType;

/**
 * homeRegis controller.
 *
 * @Route("/homeregis")
 */
class homeRegisController extends Controller
{

    /**
     * Lists all homeRegis entities.
     *
     * @Route("/", name="homeregis")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('nivaswebBundle:homeRegis')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new homeRegis entity.
     *
     * @Route("/", name="homeregis_create")
     * @Method("POST")
     * @Template("nivaswebBundle:homeRegis:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new homeRegis();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('homeregis_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a homeRegis entity.
     *
     * @param homeRegis $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(homeRegis $entity)
    {
        $form = $this->createForm(new homeRegisType(), $entity, array(
            'action' => $this->generateUrl('homeregis_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new homeRegis entity.
     *
     * @Route("/new", name="homeregis_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new homeRegis();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a homeRegis entity.
     *
     * @Route("/{id}", name="homeregis_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('nivaswebBundle:homeRegis')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find homeRegis entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing homeRegis entity.
     *
     * @Route("/{id}/edit", name="homeregis_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('nivaswebBundle:homeRegis')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find homeRegis entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a homeRegis entity.
    *
    * @param homeRegis $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(homeRegis $entity)
    {
        $form = $this->createForm(new homeRegisType(), $entity, array(
            'action' => $this->generateUrl('homeregis_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing homeRegis entity.
     *
     * @Route("/{id}", name="homeregis_update")
     * @Method("PUT")
     * @Template("nivaswebBundle:homeRegis:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('nivaswebBundle:homeRegis')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find homeRegis entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('homeregis_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a homeRegis entity.
     *
     * @Route("/{id}", name="homeregis_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('nivaswebBundle:homeRegis')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find homeRegis entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('homeregis'));
    }

    /**
     * Creates a form to delete a homeRegis entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('homeregis_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
