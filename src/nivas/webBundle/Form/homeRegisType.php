<?php

namespace nivas\webBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class homeRegisType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price')
            ->add('type')
            ->add('street')
            ->add('city')
            ->add('distict')
            ->add('restriction')
            ->add('furnished')
            ->add('imgUrl')
            ->add('other')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'nivas\webBundle\Entity\homeRegis'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'nivas_webbundle_homeregis';
    }
}
