<?php

namespace nivas\webBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * homeRegis
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class homeRegis
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="distict", type="string", length=255)
     */
    private $distict;

    /**
     * @var string
     *
     * @ORM\Column(name="restriction", type="string", length=255)
     */
    private $restriction;

    /**
     * @var string
     *
     * @ORM\Column(name="furnished", type="string", length=255)
     */
    private $furnished;

    /**
     * @var string
     *
     * @ORM\Column(name="img_url", type="string", length=255)
     */
    private $imgUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="other", type="string", length=255)
     */
    private $other;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return homeRegis
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return homeRegis
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return homeRegis
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return homeRegis
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set distict
     *
     * @param string $distict
     * @return homeRegis
     */
    public function setDistict($distict)
    {
        $this->distict = $distict;

        return $this;
    }

    /**
     * Get distict
     *
     * @return string 
     */
    public function getDistict()
    {
        return $this->distict;
    }

    /**
     * Set restriction
     *
     * @param string $restriction
     * @return homeRegis
     */
    public function setRestriction($restriction)
    {
        $this->restriction = $restriction;

        return $this;
    }

    /**
     * Get restriction
     *
     * @return string 
     */
    public function getRestriction()
    {
        return $this->restriction;
    }

    /**
     * Set furnished
     *
     * @param string $furnished
     * @return homeRegis
     */
    public function setFurnished($furnished)
    {
        $this->furnished = $furnished;

        return $this;
    }

    /**
     * Get furnished
     *
     * @return string 
     */
    public function getFurnished()
    {
        return $this->furnished;
    }

    /**
     * Set imgUrl
     *
     * @param string $imgUrl
     * @return homeRegis
     */
    public function setImgUrl($imgUrl)
    {
        $this->imgUrl = $imgUrl;

        return $this;
    }

    /**
     * Get imgUrl
     *
     * @return string 
     */
    public function getImgUrl()
    {
        return $this->imgUrl;
    }

    /**
     * Set other
     *
     * @param string $other
     * @return homeRegis
     */
    public function setOther($other)
    {
        $this->other = $other;

        return $this;
    }

    /**
     * Get other
     *
     * @return string 
     */
    public function getOther()
    {
        return $this->other;
    }
}
